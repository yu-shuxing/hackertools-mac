brew install --cask metasploit
brew install nmap
brew install liblinear
brew install libssh2
brew install luajit
brew install openssl@3
brew install pcre2
brew install perl
brew install sqlmap
brew install --cask imhex
brew install --cask burp-suite
brew install openjdk 
brew install python@3.12
brew install wget
brew install --cask idafree
brew install john
brew install --cask steam
#if you cannot run imhex . please remove it 
#install cutter: 1.brew install rizin 2.brew install --cask cutter
#remove imhex : brew uninstall imhex
#and see this https://github.com/rizinorg/cutter-plugins