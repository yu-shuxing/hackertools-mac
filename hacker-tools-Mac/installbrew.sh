export HOMEBREW_NO_INSTALL_FROM_API=1
xcode-select --install
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" 
git -C "$(brew --repo homebrew/core)" remote set-url origin https://mirrors.ustc.edu.cn/homebrew-core.git
git -C "$(brew --repo)" remote set-url origin https://mirrors.ustc.edu.cn/brew.git
brew list
brew update
brew upgrade
brew cleanup
#see https://zhuanlan.zhihu.com/p/372576355
#some errors (install step) see https://github.com/Homebrew/homebrew-cask
#run brew update-reset && brew update